<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# Aviatrix Attach Spoke To Transit Gateway

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Requirements](#requirements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

This pre-built attaches a Spoke VPC to the Transit GW Group.
The pre-built consists of a transformation to prepare the formData for the 'attachSpokeToTransitGateway' task on the workflow to perform the desired operation. 

<table><tr><td>
  <img src="./images/workflow.png" alt="workflow" width="800px">
</td></tr></table>
<!-- Write a few sentences about the Pre-Built and explain the use case(s) -->
<!-- Avoid using the word Artifact. Please use Pre-Built, Pre-Built Transformation or Pre-Built Automation -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
<!--
<table><tr><td>
  <img src="./images/workflow.png" alt="workflow" width="800px">
</td></tr></table>
-->
<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 30 seconds

## Requirements

This Pre-Built requires the following:

* Itential Automation Platform
  * `^2021.2`
* An Aviatrix controller.
* A running instance of the Itential OpenSource Aviatrix adapter, which can be found [here](https://gitlab.com/itentialopensource/adapters/cloud/adapter-aviatrix).

<!-- Unordered list highlighting the requirements of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button (as shown below).

<!-- REPLACE BELOW WITH IMAGE OF YOUR PUBLISHED PRE-BUILT -->
<!--
<table><tr><td>
  <img src="./images/install.png" alt="install" width="600px">
</td></tr></table>
-->
<!-- REPLACE ABOVE WITH IMAGE OF YOUR PUBLISHED PRE-BUILT -->

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-Built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built:

The workflow of the pre-built can be run directly from Automation Studio or as a child job or with an additional json-form supplying the values needed by the workflow.
The description of the formdata needed by the prebuilt is given in the api documentation [here](https://api.aviatrix.com/).
<table><tr><td>
  <img src="./images/formData.png" alt="formData" width="500px">
</td></tr></table>
More information on the use case can be found [here](https://docs.aviatrix.com/HowTos/transitvpc_workflow.html#join-a-spoke-gw-to-transit-gw-group).
<!-- Explain the main entrypoint(s) for this Pre-Built: Automation Catalog item, Workflow, Postman, etc. -->

## Additional Information
The parent workflow and all the associated workflows of Aviatrix Transit Network deployment are given below,
<table><tr><td>
  <img src="./images/parentWorkflow.png" alt="parentWorkflow" width="800px">
</td></tr></table>

[Aviatrix Transit Gateway Deployment](https://gitlab.com/itentialopensource/pre-built-automations/aviatrix-transit-gateway-deployment)\
[Aviatrix Create MCNA Transit](https://gitlab.com/itentialopensource/pre-built-automations/aviatrix-create-mcna-transit)\
[Aviatrix Enable Transit HA](https://gitlab.com/itentialopensource/pre-built-automations/aviatrix-enable-transit-ha)\
[Aviatrix Connect Transit Gateway To VGW](https://gitlab.com/itentialopensource/pre-built-automations/aviatrix-connect-transit-gateway-to-vgw)\
[Aviatrix Connect Transit Gateway To External Device](https://gitlab.com/itentialopensource/pre-built-automations/aviatrix-connect-transit-gateway-to-external-device)\
[Aviatrix Connect Transit Gateway To Aviatrix CloudN](https://gitlab.com/itentialopensource/pre-built-automations/aviatrix-connect-transit-gateway-to-aviatrix-cloudn)\
[Aviatrix Create Spoke Gateway](https://gitlab.com/itentialopensource/pre-built-automations/aviatrix-create-spoke-gateway)\
[Aviatrix Enable Spoke HA](https://gitlab.com/itentialopensource/pre-built-automations/aviatrix-enable-spoke-ha)\
[Aviatrix Attach ARM Native Spoke To Transit Gateway](https://gitlab.com/itentialopensource/pre-built-automations/aviatrix-attach-arm-native-spoke-to-transit-gateway)

Please use your Itential Customer Success account if you need support when using this Pre-Built.
